<?php

namespace App\Http\Controllers;

use App\Instance;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class InstanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Instance  $instance
     * @return \Illuminate\Http\Response
     */
    public function show(Instance $instance)
    {
        return view('instance.show', [
            'instance' => $instance,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Instance  $instance
     * @return \Illuminate\Http\Response
     */
    public function edit(Instance $instance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Instance  $instance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Instance $instance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Instance  $instance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Instance $instance)
    {
        //
    }

    public function image(Request $request, Instance $instance)
    {
        if ($request->hasFile('img')) {
            if($request->file('img')->isValid()) {
                try {
                    // Store the image in the private storage folder.
                    $imgPath = $request->img->store('instance_images');
                    $imgCropPath = 'crop_' . $imgPath;

                    // Get the bounding box and crop the image.
                    $rect = $instance->bounding_box;
                    $imgCrop = Image::make($request->img);
                    $width = $imgCrop->width();
                    $height = $imgCrop->height();
                    $imgCrop->crop(intval($width * $rect['width']), intval($height * $rect['height']), intval($width * $rect['x']), intval($height * $rect['y']));

                    // Store the cropped image in the private storage folder.
                    Storage::put($imgCropPath, $imgCrop->encode('png'));

                    // Delete the previous image for this instance.
                    if ($instance->image) {
                        Storage::delete([
                            $instance->image,
                            $instance->crop_image,
                        ]);
                    }

                    $instance->image = $imgPath;
                    $instance->crop_image = $imgCropPath;
                    $instance->save();

                    return response()->json([
                        'success' => true,
                    ]);
                } catch (Exception $e) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Exception: '.$e->getMessage(),
                    ], 500);
                }
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid file',
                ], 400);
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => 'No file in request',
            ], 400);
        }
    }
}
