@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('label.index') }}">Items</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $label->label }}</li>
                    </ol>
                </nav>
                <div class="card">
                    <div class="card-header">{{ $label->label }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                            <div class="card-columns">
                            @foreach($label->instance as $instance)
                                <div class="card">
                                    <img class="card-img-top" src="{{ $instance->cropped_image_path }}" alt="Card image cap">
                                    <div class="card-body">
                                        <a href="{{ route('instance.show', $instance) }}" class="btn btn-primary btn-block">View Location</a>
                                    </div>
                                    <div class="card-footer">
                                        <small class="text-muted">Last seen {{ $instance->updated_at->diffForHumans() }}</small>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        (function (labelshow, $, undefined) {

        }(window.labelshow = window.labelshow || {}, jQuery))

    </script>
@endsection
