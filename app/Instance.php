<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Instance extends Model
{
    public function label()
    {
        return $this->belongsTo('App\Label');
    }

    public function getImagePathAttribute()
    {
        return Storage::url($this->image ?: 'instance_images/no_image_available.jpg');
    }

    public function getCroppedImagePathAttribute()
    {
        return Storage::url($this->image ? 'crop_'.$this->image : 'instance_images/no_image_available.jpg');
    }

    public function getBoundingBoxAttribute()
    {
        return array(
            'x' => $this->top_x,
            'y' => $this->top_y,
            'width' => $this->bottom_x - $this->top_x,
            'height' => $this->bottom_y - $this->top_y,
        );
    }
}
