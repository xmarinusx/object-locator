@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Items</li>
                    </ol>
                </nav>
                <div class="card">
                    <div class="card-header">Items</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table id="table_items" class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Item</th>
                                <th scope="col">Last Seen</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(App\Label::whereIn('id', App\Instance::pluck('label_id')->all())->get() as $label)
                                <tr data-label="{{ $label->label }}">
                                    <td>{{ $label->label }}</td>
                                    <td>{{ $label->instance->sortByDesc('updated_at')->first()->updated_at->diffForHumans() }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        (function (labelindex, $, undefined) {
            $('#table_items').DataTable();
            $('#table_items').on('click', 'tr', function(event) {
                let label = $(this).data('label');

                if (label) {
                    window.location = '{{ route('label.show', ':slug') }}'.replace(':slug', label);
                }

            });

        }(window.labelindex = window.labelindex || {}, jQuery))

    </script>
@endsection
